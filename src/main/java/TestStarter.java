import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.mybatisflex.core.query.QueryWrapper;
import flex.FlexInitializer;
import flex.entity.FlexAccount;
import plus.PlusInitializer;
import plus.entity.PlusAccount;

import static flex.entity.table.Tables.FLEX_ACCOUNT;

public class TestStarter {

    private static final int queryCount = 1000;

    public static void main(String[] args) {

        FlexInitializer.init();
        PlusInitializer.init();


        //预热
        testFlexSelectOne();
        testPlusSelectOneWithLambda();
        testPlusSelectOne();
        testFlexPaginate();
        testPlusPaginate();
        testFlexUpdate();
        testPlusUpdate();

        long timeMillis;


        System.out.println();
        System.out.println();
        System.out.println("---------------------selectOne:");

        for (int i = 0; i < 10; i++) {
            System.out.println("---------------");

            timeMillis = System.currentTimeMillis();
            testFlexSelectOne();
            System.out.println(">>>>>>>testFlexSelectOne:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusSelectOneWithLambda();
            System.out.println(">>>>>>>testPlusSelectOneWithLambda:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusSelectOne();
            System.out.println(">>>>>>>testPlusSelectOne:" + (System.currentTimeMillis() - timeMillis));
        }


        System.out.println();
        System.out.println();
        System.out.println("---------------------selectList:");

        for (int i = 0; i < 10; i++) {
            System.out.println("---------------");

            timeMillis = System.currentTimeMillis();
            testFlexSelectTop10();
            System.out.println(">>>>>>>testFlexSelectTop10:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusSelectTop10WithLambda();
            System.out.println(">>>>>>>testPlusSelectTop10WithLambda:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusSelectTop10();
            System.out.println(">>>>>>>testPlusSelectTop10:" + (System.currentTimeMillis() - timeMillis));
        }


        System.out.println();
        System.out.println();
        System.out.println("---------------------paginate:");

        for (int i = 0; i < 10; i++) {
            System.out.println("---------------");

            timeMillis = System.currentTimeMillis();
            testFlexPaginate();
            System.out.println(">>>>>>>testFlexPaginate:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusPaginate();
            System.out.println(">>>>>>>testPlusPaginate:" + (System.currentTimeMillis() - timeMillis));
        }


        System.out.println();
        System.out.println();
        System.out.println("---------------------update:");

        for (int i = 0; i < 10; i++) {
            System.out.println("---------------");

            timeMillis = System.currentTimeMillis();
            testFlexUpdate();
            System.out.println(">>>>>>>testFlexUpdate:" + (System.currentTimeMillis() - timeMillis));

            timeMillis = System.currentTimeMillis();
            testPlusUpdate();
            System.out.println(">>>>>>>testPlusUpdate:" + (System.currentTimeMillis() - timeMillis));
        }
    }


    private static void testFlexSelectOne() {
        for (int i = 0; i < queryCount; i++) {
            FlexInitializer.selectOne();
        }
    }

    private static void testPlusSelectOneWithLambda() {
        for (int i = 0; i < queryCount; i++) {
            PlusInitializer.selectOneWithLambda();
        }
    }

    private static void testPlusSelectOne() {
        for (int i = 0; i < queryCount; i++) {
            PlusInitializer.selectOne();
        }
    }


    private static void testFlexSelectTop10() {
        for (int i = 0; i < queryCount; i++) {
            FlexInitializer.selectTop10();
        }
    }

    private static void testPlusSelectTop10WithLambda() {
        for (int i = 0; i < queryCount; i++) {
            PlusInitializer.selectTop10WithLambda();
        }
    }

    private static void testPlusSelectTop10() {
        for (int i = 0; i < queryCount; i++) {
            PlusInitializer.selectTop10();
        }
    }


    private static void testFlexPaginate() {
        for (int i = 1; i <= queryCount; i++) {
            QueryWrapper queryWrapper = new QueryWrapper()
                    .where(FLEX_ACCOUNT.ID.ge(100));
            FlexInitializer.paginate(1, 10, queryWrapper);
        }
    }

    private static void testPlusPaginate() {
        for (int i = 1; i <= queryCount; i++) {
            LambdaQueryWrapper<PlusAccount> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.ge(PlusAccount::getId, 100);
            PlusInitializer.paginate(1, 10, queryWrapper);
        }
    }

    private static void testFlexUpdate() {
        for (long i = 0; i < queryCount; i++) {
            FlexAccount flexAccount = new FlexAccount();
            flexAccount.setUserName("testInsert" + i);
            flexAccount.setNickname("testInsert" + i);
            flexAccount.addOption("key1", "value1");
            flexAccount.addOption("key2", "value2");
            flexAccount.addOption("key3", "value3");
            flexAccount.addOption("key4", "value4");
            flexAccount.addOption("key5", "value5");

            QueryWrapper queryWrapper = QueryWrapper.create()
                    .where(FLEX_ACCOUNT.ID.ge(9200))
                    .and(FLEX_ACCOUNT.ID.le(9300))
                    .and(FLEX_ACCOUNT.USER_NAME.like("admin"))
                    .and(FLEX_ACCOUNT.NICKNAME.like("admin"));

            FlexInitializer.update(flexAccount, queryWrapper);
        }
    }


    private static void testPlusUpdate() {
        for (int i = 0; i < queryCount; i++) {
            PlusAccount plusAccount = new PlusAccount();
            plusAccount.setUserName("testInsert" + i);
            plusAccount.setNickname("testInsert" + i);
            plusAccount.addOption("key1", "value1");
            plusAccount.addOption("key2", "value2");
            plusAccount.addOption("key3", "value3");
            plusAccount.addOption("key4", "value4");
            plusAccount.addOption("key5", "value5");

            LambdaUpdateWrapper<PlusAccount> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.ge(PlusAccount::getId, 9000);
            updateWrapper.le(PlusAccount::getId, 9100);
            updateWrapper.like(PlusAccount::getUserName, "admin");
            updateWrapper.like(PlusAccount::getNickname, "admin");
            PlusInitializer.update(plusAccount, updateWrapper);
        }
    }


}
